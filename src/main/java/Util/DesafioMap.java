package Util;

public interface DesafioMap {

    String URL = "https://www.grocerycrud.com/v1.x/demo/my_boss_is_in_a_hurry/bootstrap";
    String IDCOMBO = "switch-version-select";
    String IDNAME = "field-customerName";
    String IDLASTNAME = "field-contactLastName";
    String IDFIRSTNAME = "field-contactFirstName";
    String IDPHONE = "field-phone";
    String IDADDRESLINE1 = "field-addressLine1";
    String IDADDRESLINE2 = "field-addressLine2";
    String IDCITY = "field-city";
    String IDSTATE = "field-state";
    String IDPOSTALCODE = "field-postalCode";
    String IDCOUNTRY = "field-country";
    String IDEMPLOYEER = "field-salesRepEmployeeNumber";
    String IDCREDITLIMIT = "field-creditLimit";
    String IDBOTAO = "form-button-save";
    String IDMENSAGEM = "report-success";
}
