package Util;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class desafioPageObject implements DesafioMap{

    private WebDriver driver;

    public void acessarSistema() {
        System.setProperty("webdriver.chrome.driver", "drivers/chromedriver.exe");
        driver = new ChromeDriver();
        driver.get(URL);
    }

    public void mudarValorCombo (String version){
        WebElement element = driver.findElement(By.id(IDCOMBO));
        Select selectObject  = new Select(element);
        selectObject.selectByVisibleText(version);
    }

    public void clicarAddRecord () throws InterruptedException {
        WebElement botao = driver.findElement(By.xpath("//*[@id=\"gcrud-search-form\"]/div[1]/div[1]/a"));
        botao.click();
        driver.findElement(By.id(IDNAME)).sendKeys("Teste Sicredi");
        driver.findElement(By.id(IDLASTNAME)).sendKeys("Teste");
        driver.findElement(By.id(IDFIRSTNAME)).sendKeys("Juliana");
        driver.findElement(By.id(IDPHONE)).sendKeys("51 9999-9999");
        driver.findElement(By.id(IDADDRESLINE1)).sendKeys("Av Assis Brasil, 3970");
        driver.findElement(By.id(IDADDRESLINE2)).sendKeys("Torre D");
        driver.findElement(By.id(IDCITY)).sendKeys("Porto Alegre");
        driver.findElement(By.id(IDSTATE)).sendKeys("RS");
        driver.findElement(By.id(IDPOSTALCODE)).sendKeys("91000-000");
        driver.findElement(By.id(IDCOUNTRY)).sendKeys("Brasil");
        driver.findElement(By.id(IDEMPLOYEER)).sendKeys("Fixter");
        driver.findElement(By.id(IDCREDITLIMIT)).sendKeys("200");
        driver.findElement(By.id(IDBOTAO)).click();
    }

    public void validarMensagem (String mensagem) throws InterruptedException {
        Thread.sleep(5000);
        WebElement element = driver.findElement(By.id(IDMENSAGEM));
        assertEquals(element.getText(), mensagem);
    }

}