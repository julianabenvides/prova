import Util.DesafioUtil;
import Util.desafioPageObject;
import org.junit.After;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;


public class DesafioTeste extends desafioPageObject {

    private WebDriver driver;

    @Before
    public void setUp() throws Exception {
        DesafioUtil util = new DesafioUtil();
        driver = util.chromeDriverConfig();
    }

    @After
    public void CloseBrowser(){
        driver.close();
    }

    @Test

    public void desafioTeste() throws Exception {
        desafioPageObject obj = new desafioPageObject();
        obj.acessarSistema();
        obj.mudarValorCombo("Bootstrap V4 Theme");
        obj.clicarAddRecord();
        obj.validarMensagem("Your data has been successfully stored into the database. Edit Record or Go back to list");
    }
}
